import React from 'react';
import { Paper } from '@material-ui/core';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Login from '../components/login';
import SignUp from '../components/signup';

const SignInSignOutcontainer = () => {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const paperStyle ={
        width: 340,
        margin: "20px auto",
        height: '85vh'
    }


    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`full-width-tabpanel-${index}`}
                aria-labelledby={`full-width-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        );
    }
    return (
        <Paper elevation = {10} style={paperStyle}>
            <Tabs
                value={value}
                indicatorColor="primary"
                textColor="primary"
                onChange={handleChange}
                aria-label="disabled tabs example"
            >
                <Tab label="Sign In" />

                <Tab label="Sign Up" />
            </Tabs>
            <TabPanel value={value} index={0} >
                <Login />
            </TabPanel>
            <TabPanel value={value} index={1} >
                <SignUp />
            </TabPanel>
        </Paper>
    )
}

export default SignInSignOutcontainer;
