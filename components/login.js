//import { configure } from '@testing-library/dom';
import React from 'react';
import { Button, Grid, Paper, Typography } from '@material-ui/core';
import { Avatar } from '@material-ui/core';
//import { Icon } from '@material-ui/icons';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { TextField } from '@material-ui/core';
import { Link } from '@material-ui/core';


const Login=()=>{
    const paperStyle = {
        padding: 20,
        width: 300,
        height: '77vh',
        margin: "0px auto",
    }
    const avatarStyle = {
        backgroundColor: '#1910a7c9',
    }
    const buttonStyle = {
        margin: '10px 0px',
    }

    return(
        <Grid align="center">
           <Paper elevation={5} style={paperStyle}>
               <Avatar variant="circle" style={avatarStyle}>
                    <LockOutlinedIcon/>
               </Avatar>
               <h2>Sign In</h2>
               <TextField label="Username" placeholder="Enter Username" color='secondary' fullWidth required></TextField>
               <TextField label="Password" placeholder="Enter Password" type="password" color='secondary' fullWidth required ></TextField>
               
               <Button variant="contained" type= 'submit' color='secondary' fullWidth style={buttonStyle}>Sign In</Button>

                
               <Typography align="left">
                   <Link href="#" >
                        Forget Password?
                    </Link>
                </Typography>
                <Typography align="left">Do you have a account?
                   <Link href="#" >
                        Sign Up
                    </Link>
                </Typography>
           </Paper>
        </Grid>
    )
}

export default Login;
