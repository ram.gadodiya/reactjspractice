//import { configure } from '@testing-library/dom';
import React from 'react';
import { Button, Grid, Paper } from '@material-ui/core';
import { Avatar } from '@material-ui/core';
//import { Icon } from '@material-ui/icons';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { TextField } from '@material-ui/core';
//import { Link } from '@material-ui/core';
import { Radio } from '@material-ui/core';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Checkbox } from '@material-ui/core';

const SignUp = () => {
    const paperStyle = {
        padding: 20,
        width: 300,
        
        margin: "0 auto",

    }
    const avatarStyle = {
        backgroundColor: '#1910a7c9',
    }
    const buttonStyle = {
        margin: '10px 0px',
    }
    const marginTop = {
        marginTop: 8,
    }

    return (
        <Grid >
            <Paper elevation={10} style={paperStyle}>
                <Grid align="center">
                    <Avatar variant="circle" style={avatarStyle}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <h2>Sign Up</h2>
                </Grid>
                <form>
                    <TextField label="Name" placeholder="Enter Your Name" color='secondary' fullWidth required></TextField>
                    <TextField label="E-mail" placeholder="Email" color='secondary' type="email" fullWidth required></TextField>
                    <TextField label="Phone" placeholder="Phone" color='secondary' fullWidth required></TextField>
                    <FormControl component="fieldset" style={marginTop}>
                        <FormLabel component="legend">Gender</FormLabel>
                        <RadioGroup aria-label="gender" name="gender" style={{ display: 'initial' }} >
                            <FormControlLabel value="female" control={<Radio />} label="Female" />
                            <FormControlLabel value="male" control={<Radio />} label="Male" />
                        </RadioGroup>
                    </FormControl>

                    <TextField label="Password" placeholder="Enter Password" type="password" color='secondary' fullWidth required ></TextField>
                    <TextField label="Confirm Password" placeholder="Enter Password" type="password" color='secondary' fullWidth required ></TextField>
                </form>

                <FormControlLabel control={<Checkbox name="checkedA" />} label="I accpect the terms and condition" />

                <Button variant="contained" type='submit' color='secondary' fullWidth style={buttonStyle}>Sign Up</Button>

                {/* <Typography align="left">
                   <Link href="#" >
                        Forget Password?
                    </Link>
                </Typography>
                <Typography align="left">Do you have a account?
                   <Link href="#" >
                        Sign Up
                    </Link>
                </Typography> */}
            </Paper>
        </Grid>
    )
}

export default SignUp;
